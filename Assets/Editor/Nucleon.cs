﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Nucleon : MonoBehaviour
{

    public float pullForce;
    private Rigidbody nucleon;

    void Awake()
    {
        nucleon = GetComponent<Rigidbody>();
    }

    void FixedUpdate ()
    {
		nucleon.AddForce(transform.localPosition * (-pullForce));
	}
}
