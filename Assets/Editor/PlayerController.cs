﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countCubeText;
    public Text countSphereText;
    public Text countMushText;
    public Text scoreText;
    public Text winText;
    public int HowManyMush;
    public Terrain terrain;
    public TerrainData tData;
    public Text ratioText;
    public Image currentLifeBar;
    public static int pickUpAllObjects;
    public float framesToLoseLife;
    public Text gameOverText;
    public Image gameOverGray;
    public Button gameOverButton;
    public CreateSpheres createMySpheres;
    public CreateCubes createMyCubes;

    private int surfaceIndex = 0;
    private float hitpoint = 100;
    private float maxHitpoint = 100;

    private float groundWidth, groundHeight;
    private Vector3 terrainPos;
    private Rigidbody rb;
    private int countCube, countSphere, countMush;
    private double gameStartTime;


    void Start()
    {
        Time.timeScale = 1;
        tData = terrain.terrainData;

        groundWidth = tData.heightmapWidth*2;
        groundHeight = tData.heightmapHeight*2;
        terrainPos = terrain.transform.position;
        rb = GetComponent<Rigidbody>();
        countCube = 0;
        countSphere = 0;
        countMush = 0;
        SetcountText();
        winText.text = "";
        UpdateHealthBar();
        framesToLoseLife *= 1000;
        gameOverButton.onClick.AddListener(OnPressGameOverButton);
    }

    void Update()
    {
        surfaceIndex = GetMainTexture(transform.position);
        WaterCollider();
        scoreText.text = "Punkty: " + pickUpAllObjects.ToString();
        float currentFrameCount = Time.frameCount;
        /* **************  Lose life after time   *************** */

        if (currentFrameCount >= framesToLoseLife)
        {
            if (framesToLoseLife >= 6000)
                TakeDamage(5);
            
            else
                TakeDamage(2);
            
            framesToLoseLife += 1000;
        }

        /* ************************  gameObject out of range ************************** */
        print(rb.transform.position.z);
        if (rb.transform.position.z <= -485f || rb.transform.position.z >= 485f || rb.transform.position.x <= -485f || rb.transform.position.x >= 485f)
        {
            OnPressGameOverButton();
        }

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        
        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
        rb.AddForce(movement * speed);
        GameOver();


    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUpCube") || other.gameObject.CompareTag("PickUpCubeMother"))
        {
            other.gameObject.SetActive(false);
            countCube++;            
            DuplicateMushroom();
            HealDamage(0.25f);
            pickUpAllObjects++;
            SetcountText();
        }
        else if (other.gameObject.CompareTag("PickUpSphere") || other.gameObject.CompareTag("PickUpSphereMother"))
        {
            other.gameObject.SetActive(false);
            countSphere++;
            
            DuplicateMushroom();
            HealDamage(0.25f);
            pickUpAllObjects++;
            SetcountText();
        }
        else if(other.gameObject.CompareTag("PickUpMush") || other.gameObject.CompareTag("PickUpMushMother"))
        {
            other.gameObject.SetActive(false);
            countMush++;

            HealDamage(0.5f);
            pickUpAllObjects +=2;
            SetcountText();
        }
        else if (other.gameObject.CompareTag("PickUpFractal") || other.gameObject.CompareTag("PickUpFractalMother"))
        {
            other.gameObject.SetActive(false);
            pickUpAllObjects+=5;
            HealDamage(1.75f);
        }

        else if (other.gameObject.CompareTag("Fire"))
        {
            TakeDamage(8);
        }

        else if (other.gameObject.CompareTag("Nucleon"))
        {
            HealDamage(1);
        }
    }

    void SetcountText()
    {
        countCubeText.text = "Zebrane sześciany: " + countCube.ToString();
        countSphereText.text = "Zebrane kule: " + countSphere.ToString();
        countMushText.text = "Zebrane grzybki: " + countMush.ToString();
    }

// duplikowanie grzybkow po zebraniu ocubes i spheres
    void DuplicateMushroom()
    {
        for(int i =0; i<HowManyMush; i++)
        {

            List<Object> newMush = new List<Object>();
            List<float> width = new List<float>();
            List<float> height = new List<float>();
            List<Vector3> signPosition = new List<Vector3>();

            for (int iter = 0; iter < 3; iter++)
            {
                newMush.Add(Resources.Load("mush"+(iter+1)));
                width.Add(Random.Range(-1 * (groundWidth / 2), groundWidth / 2));
                height.Add(Random.Range(-1 * (groundHeight / 2), groundHeight / 2));
                signPosition.Add(new Vector3(width[iter], 0, height[iter]));
                Vector3 tempPos = signPosition[iter];
                tempPos.y = Terrain.activeTerrain.SampleHeight(signPosition[iter]) + Terrain.activeTerrain.GetPosition().y;
                signPosition[iter] = tempPos;
            }

            for (int iter = 0; iter < 3; iter++)
            {
                Instantiate(newMush[iter], new Vector3(width[iter], signPosition[iter].y, height[iter]), Quaternion.identity);
            }
        }
    }

    /* *********  Health Bar   ***********/
    private void UpdateHealthBar()
    {
        float ratio = hitpoint / maxHitpoint;
        currentLifeBar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void TakeDamage(float damage)
    {
        hitpoint -= damage;
        if (hitpoint < 0)
        {
            hitpoint = 0;
        }
        UpdateHealthBar();
    }

    private void HealDamage(float heal)
    {
        hitpoint += heal;
        if (hitpoint > maxHitpoint)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthBar();
    }

    /* **************  Collider with water material   *************** */

    private void WaterCollider()
    {
        if (tData.splatPrototypes[surfaceIndex].texture.name == "waterfall")
        {
            TakeDamage(1);
        }

    }

     private float[] GetTextureMix(Vector3 WorldPos)
        {
       
            int mapX = (int)(((WorldPos.x - terrainPos.x) / tData.size.x) * tData.alphamapWidth);
            int mapZ = (int)(((WorldPos.z - terrainPos.z) / tData.size.z) * tData.alphamapHeight);

            float[,,] splatmapData = tData.GetAlphamaps(mapX, mapZ, 1, 1);

            float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

            for (int n = 0; n < cellMix.Length; n++)
            {
                cellMix[n] = splatmapData[0, 0, n];
            }
            return cellMix;
        }
    private int GetMainTexture(Vector3 WorldPos)
        {   
            float[] mix = GetTextureMix(WorldPos);

            float maxMix = 0;
            int maxIndex = 0;

            for (int n = 0; n < mix.Length; n++)
            {
                if (mix[n] > maxMix)
                {
                    maxIndex = n;
                    maxMix = mix[n];
                }
            }
            return maxIndex;
        }
/************************ Game Over ***********************/

    private void GameOver()
    {
        if (hitpoint == 0)
        {
            Time.timeScale = 0;

            gameOverText.text = 
                "Koniec gry!" + 
                Environment.NewLine + 
                "Zdobyte punkty: "
                + pickUpAllObjects.ToString();

            gameOverText.gameObject.SetActive(true);
            gameOverGray.gameObject.SetActive(true);
            gameOverButton.gameObject.SetActive(true);
        }
    }

    private void OnPressGameOverButton()
    {
        gameOverText.gameObject.SetActive(false);
        gameOverGray.gameObject.SetActive(false);
        gameOverButton.gameObject.SetActive(false);
        hitpoint = 100;
        pickUpAllObjects = 0;
        this.Start();
        framesToLoseLife = 2000;
        Vector3 newPos = new Vector3(209f, 35f, -267f);
        transform.position = newPos;
        transform.Rotate(0f,0f,0f);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        GameObject[] firesArray = GameObject.FindGameObjectsWithTag("Fire");
        GameObject[] fractalsArray = GameObject.FindGameObjectsWithTag("PickUpFractal");
        GameObject[] spheresArray = GameObject.FindGameObjectsWithTag("PickUpSphere");
        GameObject[] cubesArray = GameObject.FindGameObjectsWithTag("PickUpCube");
        GameObject[] mushesArray = GameObject.FindGameObjectsWithTag("PickUpMush");

        foreach (GameObject go in firesArray) Destroy(go); // go.SetActive(false);
        foreach (GameObject go in fractalsArray) Destroy(go); // go.SetActive(false);
        foreach (GameObject go in spheresArray)  Destroy(go); // go.SetActive(false); 
        foreach (GameObject go in cubesArray) Destroy(go); // go.SetActive(false);
        foreach (GameObject go in mushesArray) Destroy(go); // go.SetActive(false);

        FPSCounter resetCounter = new FPSCounter();
        resetCounter.fpsMin = 1000;
        resetCounter.fpsMax = -1;
        resetCounter.avgIndex = 0;
        
        
        createMySpheres.Start();
        createMyCubes.Start();

    }
}

