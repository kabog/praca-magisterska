﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuplicateFractal : MonoBehaviour {

    
    public int circleRadius;

    private int n = 1;

    void Update()
    {
        if (PlayerController.pickUpAllObjects > n)
        {
            int how_many = 14; //Random.Range(1, 15);
            DuplicateFractals();
            n += how_many;
        }
    }

    void DuplicateFractals()
    {
        int howManyFractals = 1; //Random.Range(0, 3);
        var center = transform.position;
        for (int i = 0; i < howManyFractals; i++)
        {
            var pos = RandomCircle(center, circleRadius);
            Instantiate(Resources.Load("PickUpFractal"), pos, Quaternion.identity);
        }
    }

    private Vector3 RandomCircle(Vector3 center, float radius)
    {
        var ang = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.y = 0;
        Vector3 tempPos = pos;
        tempPos.y = Terrain.activeTerrain.SampleHeight(pos) +
        Terrain.activeTerrain.GetPosition().y;
        tempPos.y += 1f;
        pos = tempPos;

        return pos;
    }
}
