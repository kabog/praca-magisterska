﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFractal : MonoBehaviour
{

    public List<Mesh> mesh;
    public Material material;
    public int maxChildNum;
    public float resizeChild;
    public float probability;
    public float maxRotationSpeed;
    public float maxRotationAngle;

    private int childNumber;
    private Material[,] fracMaterials;
    private float rotationSpeed;

    public void Start()
    {
        gameObject.AddComponent<MeshFilter>().mesh = mesh[Random.Range(0, mesh.Count)];
        if (fracMaterials == null)
            ApplyMaterials();
        gameObject.AddComponent<MeshRenderer>().material = fracMaterials[childNumber, Random.Range(0,2)];
        gameObject.AddComponent<BoxCollider>().isTrigger = true;
        rotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
        transform.Rotate(Random.Range(-maxRotationAngle, maxRotationAngle), 0f, 0f);
        if (childNumber < maxChildNum)
        {
            StartCoroutine(CreateChild());
        }
    }

    private void Update()
    {
        /*if (gameObject.GetComponent<Renderer>().isVisible) */
        {
            transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);
        }
    }

    void InitChilds(CreateFractal parent, Vector3 direction, Quaternion orientation)
    {
        gameObject.AddComponent<BoxCollider>().isTrigger = true;
        //gameObject.AddComponent<Rigidbody>().isKinematic = true;
        mesh = parent.mesh;
        fracMaterials = parent.fracMaterials;
        maxChildNum = parent.maxChildNum;
        transform.parent = parent.transform;
        childNumber = parent.childNumber + 1;
        resizeChild = parent.resizeChild;
        probability = parent.probability;
        maxRotationSpeed = parent.maxRotationSpeed;
        maxRotationAngle = parent.maxRotationAngle;
        transform.localScale = Vector3.one*resizeChild;
        transform.localPosition = (resizeChild * resizeChild + resizeChild) *direction;
        transform.localRotation = orientation;
    }

    private IEnumerator CreateChild()
    {
        List<Vector3> move = new List<Vector3>()
        {
            Vector3.up, Vector3.right, Vector3.left, Vector3.forward, Vector3.back
        };
        List<Quaternion> angle = new List<Quaternion>()
        {

            Quaternion.identity, Quaternion.Euler(0f, 0f, -90f), Quaternion.Euler(0f, 0f, 90f),
            Quaternion.Euler(90f, 0f, 0f), Quaternion.Euler(-90f, 0f, 0f)
        };

        for (int i = 0; i < 5; i++)
        {
            if (Random.value < probability)
            {
                yield return new WaitForSeconds(Random.Range(0f, 0.5f));
                new GameObject("Child").AddComponent<CreateFractal>()
                    .InitChilds(this, move[i], Quaternion.Euler(angle[i].x, angle[i].y, angle[i].z));
            }
        }
    }

    
    private void ApplyMaterials()
    {
        MyMain col = new MyMain();
        fracMaterials =  new Material[ maxChildNum + 1, 2] ;


        for (int i = 0; i < maxChildNum; i++)
        {
            float t = i / (maxChildNum - 1f);
            t *= t;
            fracMaterials[i, 0] = new Material(material);
            fracMaterials[i, 1] = new Material(material);
            fracMaterials[i, 0].color = Color.Lerp(new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]),
                new Color( col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2] ), t);
            fracMaterials[i, 1].color = Color.Lerp(new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]),
                new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2] ), t);
        }
        fracMaterials[maxChildNum, 0] = new Material(material);
        fracMaterials[maxChildNum, 1] = new Material(material);
        fracMaterials[maxChildNum, 0].color = new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]);
        fracMaterials[maxChildNum, 1].color = new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]);
    }



}
