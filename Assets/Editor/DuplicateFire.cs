﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuplicateFire : MonoBehaviour
{

    public int circleRadius;

    private int n = 5;


    void Update()
    {
        if (PlayerController.pickUpAllObjects > n)
        {
            int how_many = 5; //Random.Range(1, 15);
            DuplicateFires();
            n += how_many;
        }
    }

    void DuplicateFires()
    {
        int howManyFires = 8; // Random.Range(5, 10);
        var center = transform.position;
        for (int i = 0; i < howManyFires; i++)
        {
            var pos = RandomCircle(center, circleRadius);
            Instantiate(Resources.Load("FireCreate"), pos, Quaternion.identity);
        }
    }

    private Vector3 RandomCircle(Vector3 center, float radius)
    { 
        var ang = Random.value * 360;
        Vector3 pos; 
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.y = 0;
        Vector3 tempPos = pos;
        tempPos.y =  Terrain.activeTerrain.SampleHeight(pos) +
        Terrain.activeTerrain.GetPosition().y;
        tempPos.y += 1f;
        pos = tempPos;

        return pos; 
    }
}