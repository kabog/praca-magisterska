﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CreateCubes : MonoBehaviour
{

    public int howMany;
    private List<GameObject> cube;
    private Rigidbody rb;
    public Terrain terrain;
    public TerrainData tData;
    private float groundWidth, groundHeight;

    public void Start()
    {
        tData = terrain.terrainData;
        cube = new List<GameObject>();
        groundWidth = tData.heightmapWidth*2;
        groundHeight = tData.heightmapHeight*2;
        CreateCubesFunc();
    }

    void Update ()
    {
        
        transform.Rotate(new Vector3(15, 30, 45)*Time.deltaTime);
        //if (gameObject.GetComponent<Renderer>().isVisible)
        {
            for (int i = 0; i < howMany; i++)
            {
                cube[i].transform.Rotate(new Vector3(45, 45, 45)*Time.deltaTime);
            }
        }

    }

    void CreateCubesFunc()
    {
        for (int i = 0; i < howMany; i++)
        {
            float width = Random.Range(-1*(groundWidth/2), groundWidth/2);
            float height = Random.Range(-1*(groundHeight/2), groundHeight/2);
            GameObject singleCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Vector3 signPosition = new Vector3(width, 0, height);
            signPosition.y = Terrain.activeTerrain.SampleHeight(signPosition) + Terrain.activeTerrain.GetPosition().y;
            signPosition.y += 0.5f;

            singleCube.transform.position = new Vector3(width, signPosition.y, height);
            singleCube.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            singleCube.gameObject.tag = "PickUpCube";
            singleCube.GetComponent<BoxCollider>().isTrigger = true;
            //singleCube.GetComponent<StaticOcclusionCulling>();
            cube.Add(singleCube);
        }
        ColorizeCubes();
    }

    void ColorizeCubes()
    {
        MyMain col = new MyMain();
        for (int i = 0; i < howMany; i++)
        {
                cube[i].GetComponent<MeshRenderer>().material.color = new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]);    
        }
    }
}
