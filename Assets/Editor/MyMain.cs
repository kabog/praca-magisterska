﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMain : MonoBehaviour {

    public int r { get { return (int) Random.Range(0, 255)/255; }  }
    public int g { get { return (int)Random.Range(0, 255) / 255; } }
    public int b { get { return (int)Random.Range(0, 255) / 255; } }

    public List<float> RandomColors()
    {
        List<float> rgb = new List<float>()
        {
            Random.Range((int)0, (int)255) / 255f ,
            Random.Range((int)0, (int)255) / 255f ,
            Random.Range((int)0, (int)255) / 255f
        };
        return rgb;
    }
}
