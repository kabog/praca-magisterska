﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCounter : MonoBehaviour {

    public int framePerSec { get; private set; }
    public int avgFramePerSec { get; private set; }
    public int minframePerSec { get; private set; }
    public int maxFramePerSec { get; private set; }

    public int avgRange = 60;

    public int[] fpsAvg;
    public int avgIndex = 0;
    public int fpsMax = -1;
    public int fpsMin = 1000;

    void Start()
    {
        fpsAvg = new int[avgRange];
    }

    public void Update ()
	{
        framePerSec = (int) (1f/ Time.unscaledDeltaTime);
	    AverageSum();
        AverageMinMaxCalulation();
	}

    void AverageSum()
    {
        fpsAvg[avgIndex++] = (int)(1f / Time.unscaledDeltaTime);
        if (avgIndex >= avgRange)
            avgIndex = 0;
    }

    void AverageMinMaxCalulation()
    {
        int sum = 0;
        for (int i = 0; i < avgRange; i++)
        {
            sum += fpsAvg[i];
        }
        avgFramePerSec = sum/avgRange;

        if (fpsMax < framePerSec)
        {
            fpsMax = framePerSec;
            maxFramePerSec = fpsMax;
        }
        if (fpsMin == 0)
            fpsMin = 1000;
        if (fpsMin > framePerSec)
        {
            fpsMin = framePerSec;
            minframePerSec = fpsMin;
        }

    }


}
