﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CreateSpheres : MonoBehaviour {

    public int howMany;
    private List<GameObject> sphere;
    private Rigidbody rb;
    public Terrain terrain;
    public TerrainData tData;

    private float groundWidth, groundHeight;

    public void Start()
    {

        tData = terrain.terrainData;
        sphere = new List<GameObject>();
        groundWidth = tData.heightmapWidth*2;
        groundHeight = tData.heightmapHeight*2;

        CreateSpheresFunc(howMany);
    }

/*    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
        for (int i = 0; i < howMany; i++)
        {
            sphere[i].transform.Rotate(new Vector3(45, 45, 45) * Time.deltaTime);
        }

    }*/

    void CreateSpheresFunc(int howMany)
    {
        for (int i = 0; i < howMany; i++)
        {
            float width = Random.Range(-1 * (groundWidth / 2), groundWidth / 2);
            float height = Random.Range(-1 * (groundHeight / 2), groundHeight / 2);
            GameObject singleSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            Vector3 signPosition = new Vector3(width, 0, height);
            signPosition.y = Terrain.activeTerrain.SampleHeight(signPosition) + Terrain.activeTerrain.GetPosition().y;
            signPosition.y += 0.5f;

            singleSphere.transform.position = new Vector3(width, signPosition.y, height);
            singleSphere.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            singleSphere.gameObject.tag = "PickUpSphere";
            singleSphere.GetComponent<SphereCollider>().isTrigger = true;
            sphere.Add(singleSphere);
        }
        ColorizeSpheres();
    }
    

    void ColorizeSpheres()
    {
        MyMain col = new MyMain();
        for (int i = 0; i < howMany; i++)
        {
            sphere[i].GetComponent<MeshRenderer>().material.color = new Color(col.RandomColors()[0], col.RandomColors()[1], col.RandomColors()[2]);
        }
    }
}