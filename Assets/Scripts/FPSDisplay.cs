﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(FPSCounter))]
public class FPSDisplay : MonoBehaviour
{

    public Text fpsCurrent, fpsAverage, fpsMin, fpsMax;


    public FPSCounter fpsCounter = new FPSCounter();
    public FPSCounter avgFpsCounter = new FPSCounter();
    public FPSCounter maxFpsCounter = new FPSCounter();
    public FPSCounter minFpsCounter = new FPSCounter();

    void Awake()
    {
        fpsCounter = GetComponent<FPSCounter>();
        avgFpsCounter = GetComponent<FPSCounter>();
        maxFpsCounter = GetComponent<FPSCounter>();
        minFpsCounter = GetComponent<FPSCounter>();
    }

    public void Update()
    {
        fpsCurrent.text = fpsCounter.framePerSec.ToString();
        fpsAverage.text = avgFpsCounter.avgFramePerSec.ToString();
        fpsMin.text = minFpsCounter.minframePerSec.ToString();
        fpsMax.text = maxFpsCounter.maxFramePerSec.ToString();
    }
}
