﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NucleonMaintanance : MonoBehaviour
{

    public float waitForSpawns;
    public float spawnsDistance;
    public int howManyProtNeut;
    public Nucleon[] protonsNeutrons;

    private float howLongFronLastSpawn;
    private int i =0;
    private void FixedUpdate()
    {
            howLongFronLastSpawn += Time.deltaTime;
            if (howLongFronLastSpawn >= waitForSpawns)
            {
                howLongFronLastSpawn -= waitForSpawns;
            if(i< howManyProtNeut)
            { 
                SpawnProtonsNeutrons();
                i++;
            }
        }
    }

    private void SpawnProtonsNeutrons()
    {
        Nucleon protNeut = protonsNeutrons[Random.Range(0, protonsNeutrons.Length)];
        Nucleon spawn = Instantiate<Nucleon>(protNeut);
        spawn.transform.localPosition = Random.onUnitSphere*spawnsDistance;
    }
}
